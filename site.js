(function($) {
    $(document).ready(function() {

		$(function videoToggles() {
			var video_player = $('#video-player').get(0);

			$('.play-button').click(function() {
				$('.video-modal').addClass('vm-open');
				video_player.play();
				video_player.controls = true;
			});
	
			$('.close-modal').click(function() {
				$('.video-modal').removeClass('vm-open');
				video_player.pause();
			});
		});

		// accessible accordion block - controls and aria events for screenreaders
		$(function accordionBlock() {
			$('.accordion-content').each(function() {
				$(this).hide();
			});

			$('.accordion-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.toggleClass('remove-border');
					$this.removeClass('target');
					$this.attr('aria-pressed', 'true');
					$this.next('.accordion-content').slideToggle(350);
					$this.next('.accordion-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.accordion-content:first').slideToggle(350, function() {
						$('.accordion-block-title').addClass('target');
						$('.accordion-block-title').attr('aria-pressed', 'false');
						$('.accordion-content').attr('aria-expanded', 'false');
						$this.toggleClass('remove-border');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

	}); // end Document.Ready
})(jQuery);